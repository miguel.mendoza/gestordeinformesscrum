import React, { Component } from 'react';
import { Router, Route, browserHistory } from 'react-router';
import Login from './components/Login';
import Index from './components/Index';
import NewForm from './components/NewForm';
import './App.css';
import ExampleForm from './components/exampleForm/exampleForm';
import ExampleForm2 from './components/exampleForm2/exampleForm2';

import ViewForm from './components/viewForm/ViewForm';
import CreateTeam from './components/CreateTeam/CreateTeam';
import AddPerson from './components/AddPerson/AddPerson';
import MyTeams from './components/MyTeams/MyTeams';
import Manage from './components/manage/manage';
import ManageTeams from './components/ManageTeams/ManageTeams';
import ManagePerson from './components/managePerson/ManagePerson';
import ViewPerson from './components/viewPerson/viewPerson';
class App extends Component{
   render(){
    return( 
 
      <Router history={browserHistory}>
         <Route exact path="/" component={Login}/>
         <Route exact path="/index" component={Index} />
         <Route exact path="/nuevo" component={NewForm}/>
         <Route exact path="/llenarFormulario" component={ExampleForm}/>
         <Route exact path="/template2" component={ExampleForm2}/>   
         <Route exact path="/verRespuestas" component={ViewForm}/>
         <Route exact path="/nuevoEquipo" component={CreateTeam}/>
         <Route exact path="/editarEquipo" component={MyTeams}/>
         <Route exact path="/equipos" component={ManageTeams}/>
         <Route exact path="/personas" component={ManagePerson}/>
         <Route exact path="/nuevaPersona" component={AddPerson}/>
         <Route exact path="/verPersona" component={ViewPerson}/>
         <Route exact path="/administrar" component={Manage}/>
         <Route exact path="/anadirPersona" component={AddPerson}/>


      </Router>
    ); 
  }
}

export default App;
