import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import './TipsForaAnEffectiveOneOnOne.css';

export default class TipsForaAnEffectiveOneOnOne extends Component {
  render() {
    return(
      <div  className='paperTip'>       
        <Paper>
          <Typography variant="h5" component="h3">
          Consejos para una reunión personal efectiva:
          </Typography>
          <ul >
            <li className='list'>
            <Typography component="p">
            Pase tiempo preparándose en base a sus conversaciones anteriores con el empleado.            </Typography>    
            </li>
            <li className='list'>
            <Typography component="p">
            Conviértalo en una conversación bidireccional, no en una conferencia unidireccional.            </Typography>    
            </li>
            <li className='list'>
            <Typography component="p">
            Haga preguntas sobre el trabajo que el empleado está haciendo para alentar la discusión.
            </Typography>    
            </li>
            <li>
            <Typography component="p">
            Mantenlo genuino y enfocado; aléjate de tu computadora y pon tu teléfono en silencio.            </Typography>    
            </li>
            <li className='list'>
            <Typography component="p">
            Asegúrese de preguntar sobre los desafíos y cómo puede ayudar a los empleados a superarlos.
            </Typography>    

            </li>
          </ul>
          </Paper>
      </div>
    )
  }
}