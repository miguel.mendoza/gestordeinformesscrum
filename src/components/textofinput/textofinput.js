import React,{Component} from 'react'
import './textofinput.css'
import Button from '@material-ui/core/Button';
import { TextField } from '@material-ui/core';

export default class Textofinput extends Component {
  
    _remove(){
      if(this.props.onRemove )
          this.props.onRemove();
    }
  
    
render(){

   return (
   <div>
  
      <Button color="secondary" className="remove" onClick={this._remove.bind(this)}>X</Button>

        <TextField
            id="0"
            label="Ingrese la opcion"
            fullWidth
            margin="normal"
            variant="outlined"
            onChange={this.props.onChange}
            respuesta={this.props.respuesta}
            InputLabelProps={{
              shrink: true,
            }}
          />
      
  </div>
   )
}
}


