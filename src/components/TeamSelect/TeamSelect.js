import React from 'react'
import './TeamSelect.css'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
  root: {
  
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    display: 'flex',
    flexWrap: 'wrap',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function TeamSelect(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    id: '',
    name: 'Equipo',
  });
  
  function handleChange(event) {
    setValues(oldValues => ({
      ...oldValues,
      [event.target.name]: event.target.value,
      
    }));
    props.onChange(event.target);
    
  }
    function generarLista(){
      let i=-1;
      return props.ids.map((id) =>
      <MenuItem  key={i++} value={parseInt(id, 10)} >{props.nombresDeEquipos[i]}</MenuItem>
      )
    }
    
  return (
    <FormControl autoComplete="off" className={classes.formControl}>
      <InputLabel htmlFor="age-simple">Equipo</InputLabel>
      <Select
        value={values.id}
        onChange={handleChange}
        inputProps={{
          name: 'id',
          id: 'age-simple',
        }}
      >
        {generarLista()}
       
      </Select>
      </FormControl>
  );
} 

