import React from 'react'
import './inputQuestion.css'
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import CheckboxInput from '../checkboxInput/checkboxInput';
import RadioInput from '../radioInput/radioInput';
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 600,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function InputQuestion(props) {
  const classes = useStyles();
  let [value, setValue] = React.useState(props);
  
  function handleChange(event) {
    setValue(event.target.value);
  }
 
 
  return (
<div>
<form className={classes.root} autoComplete="off">
    <FormControl  component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Selecione Input</FormLabel>
        <RadioGroup
          aria-label="Inputs"
          name="inputs1"
          className={classes.group}
          onChange={handleChange}
          >
          <FormControlLabel onClick={props.onClick} value="text" control={<Radio />} label="Respuesta corta" />
          <FormControlLabel onClick={props.onClick} value="textArea" control={<Radio />} label="Parrafo" />
          <FormControlLabel onClick={props.onClick} value="checkbox" control={<Radio />} label="Seleccion Multiple" />
          <FormControlLabel onClick={props.onClick} value="radio" control={<Radio />} label="Casilla de Verificacion" />
        </RadioGroup>
      </FormControl>
  </form>
    <div>
        {value==="checkbox" && 
          <CheckboxInput respuesta={props.respuesta} index={props.index} indexOpcion={props.indexOpcion} onChange={props.onChange}> </CheckboxInput>}
            
        {value==="radio" &&
          <RadioInput  respuesta={props.respuesta} index={props.index} indexOpcion={props.indexOpcion} onChange={props.onChange}></RadioInput>}
    </div>
</div>

 );
}

