import React, { Component } from 'react'
import './QuestionTemplate.css'
import Text from '../Text/Text';
import TextArea from '../TextArea/TextArea';
import CheckBox from '../CheckBox/CheckBox';
import RadioButton from '../radioButton/radioButton';
export default class QuestionTemplate extends Component {
  
  render() {
    switch(this.props.input) {
      case 'textArea':
        return ( <div>
          <TextArea pregunta={this.props.pregunta} onChange={this.props.onChange} respuesta={this.props.respuesta} />
          </div>)  
     case 'checkbox':
      return ( <div>
        <CheckBox index={this.props.index} opciones={this.props.opciones} pregunta={this.props.pregunta} onChange={this.props.onChange} respuesta={this.props.respuesta} />
        </div>)    
      case 'radio':
        return ( <div>
          <RadioButton index={this.props.index} opciones={this.props.opciones} pregunta={this.props.pregunta} onClick={this.props.onClick} respuesta={this.props.respuesta} />
          </div>)  
      default:
        return( <div>
          <Text pregunta={this.props.pregunta}  onChange={this.props.onChange} respuesta={this.props.respuesta}/>
          </div>)
  }
  }
   
}