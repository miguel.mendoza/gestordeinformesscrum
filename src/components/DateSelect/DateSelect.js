import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    display: 'flex',
    flexWrap: 'wrap-reverse',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function DateSelect(props) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    id: "",
    name: 'Fecha',
  });
  
  function handleChange(event) {
    props.onChange(event.target);
    setValues(oldValues => ({
      ...oldValues,
      [event.target.name]: event.target.value,
      
    }));
    
  }
  function formatearFecha(fecha){
    return JSON.stringify(fecha)[9]+JSON.stringify(fecha)[10]+JSON.stringify(fecha)[8]+JSON.stringify(fecha)[6] +JSON.stringify(fecha)[7]+JSON.stringify(fecha)[5]+JSON.stringify(fecha)[1]+JSON.stringify(fecha)[2]+ JSON.stringify(fecha)[3]+JSON.stringify(fecha)[4];
  }
    function generarLista(){
      let i=-1;
      return props.fecha.map((fecha) =>
      <MenuItem  key={i++} value={i} >{formatearFecha(fecha)}</MenuItem>
      )
    }
    
  
    
  return (
    <FormControl autoComplete="off" className={classes.formControl}>
      <InputLabel htmlFor="age-simple">Fecha de la entrevista</InputLabel>
      <Select
        value={values.id}
        onChange={handleChange}
        inputProps={{
          name: 'id',
          id: 'age-simple',
        }}
      >
        {generarLista()}
       
      </Select>
      </FormControl>
  );
} 

