import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import './BasicQuestions.css';
import TeamSelect from '../TeamSelect/TeamSelect';
import PersonSelect from '../PersonSelect/PersonSelect';

export default class BasicQuestions extends Component {

  constructor(props){
    super(props);
    this.state = {
      datosPersonales: ['','',''],
      id: Array(0),
      nombreDelEquipo: Array(0),
      personas: Array(0),
      idPersonas: Array(0),
    };
  }
    componentDidMount(){
      this.recuperarGrupos();
    }
    recuperarGrupos(){
      let token= sessionStorage.Token;
      let username= sessionStorage.Nombre;
      let url= 'http://localhost:8090/getAllTeams?username='+username;
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
         .then((gruposRecuperados) => {
           for(let i=0;i<JSON.parse(gruposRecuperados).length; i++){
            let id =this.state.id.slice();
            id.push(JSON.parse(gruposRecuperados)[i].id);
            let nombreDelEquipo =this.state.nombreDelEquipo.slice();
            nombreDelEquipo.push(JSON.parse(gruposRecuperados)[i].name);
            this.setState({ 
              id: id ,
              nombreDelEquipo: nombreDelEquipo
            });
           }
        },
          (error) => {
            console.log(error);
          });
    }
    recuperarPersonas(){
      let token= sessionStorage.Token;
      let url= 'http://localhost:8090/getAllPersons?id=1';
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
         .then((personasRecuperadas) => {
           let personas= Array(0);
           let idPersonas= Array(0);
          for(let i=0;i<JSON.parse(personasRecuperadas).length; i++){
            idPersonas.push(JSON.parse(personasRecuperadas)[i].id);
            personas.push(JSON.parse(personasRecuperadas)[i].name+" "+JSON.parse(personasRecuperadas)[i].lastName);
            this.setState({ 
              idPersonas: idPersonas ,
              personas: personas
            });
          }
           console.log(this.state.personas);
           console.log(this.state.idPersonas);
        },
          (error) => {
            console.log(error);
          });
    }
    onChangeData(i,e){ 
      var datosPersonales=this.state.datosPersonales.slice() ; 
      if(i===0){
        for(i=0; i<this.state.id.length;i++){
          if(e.value===this.state.idPersonas[i]){
             datosPersonales[0]=this.state.personas[i];
             this.setState({datosPersonales: datosPersonales});
           }  
         }
     
        }else
           if(i===1){

             for(i=0; i<this.state.id.length;i++){
               if(e.value===this.state.id[i]){
                 datosPersonales[1]=this.state.nombreDelEquipo[i];
                 this.setState({datosPersonales: datosPersonales});
                 
                }  
              }
            
      }
      else{
          datosPersonales[2] = e.target.value;  
          this.setState({datosPersonales: datosPersonales});
          this.props.callback(this.state.datosPersonales);
        }
      this.recuperarPersonas();
    } 
    
  render(){
    return(
      <div className='BasicQuestions'>
        <h4>Datos del entrevistado</h4>
      <TeamSelect ids={this.state.id} nombresDeEquipos={this.state.nombreDelEquipo} onChange={this.onChangeData.bind(this,1)} ></TeamSelect> 
       <PersonSelect ids={this.state.idPersonas} personas={this.state.personas} onChange={this.onChangeData.bind(this,0)} ></PersonSelect> 
        <TextField
            id="outlined-full-width"
            label="Fecha de la entrevista"
            type="date"
            defaultValue={this.state.date}
            InputLabelProps={{
              shrink: true,
            }}
            fecha={this.props.fecha}
            onChange={this.onChangeData.bind(this,2)}
         />        
      </div>
    );
  }
}

