import React, { Component } from 'react'
import '../AddCheck/AddCheck.css'
import Button from '@material-ui/core/Button';

export default class AddCheck extends Component {
      _add(){
        if(this.props.onClick)
            this.props.onClick();
    }

    render(){
        return (
          <Button className="AddButton" onClick={this._add.bind(this)}>Añadir Respuesta</Button>
          
        )
    }
}