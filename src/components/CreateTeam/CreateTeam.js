import React, { Component } from 'react';
import './CreateTeam.css';
import BackButton from '../backButton/backButton';
import NavBar from '../NavBar';
import { Paper, TextField, Button } from '@material-ui/core';

export default class CreateTeam extends Component {
  constructor(props){
    super(props);
    this.state = {
      nombreDeEquipo: '',
     };
     this.flagNombre=this.flagNombre;
     this.flagNombre=true;
    this.onChange=this.onChange.bind(this);
    this.handleSave=this.handleSave.bind(this);
  }
  loged() {
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/loged?';
    let name= sessionStorage.Nombre;
    if(name===undefined){
      url+="username=null";  
    }
    else{
    url+="username="+name; 
    } 
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
    .then((data) => {
      if(data==="true"){
          return true;
      }else{  
        window.location="/";  
      }
    },
    (error) => {
      console.log(error);
    });
  }
  componentDidMount(){
    this.loged();

  }

    onChange(e) { 
      var nombreDeEquipo=this.state.nombreDeEquipo;
      nombreDeEquipo = e.target.value; 
      if(nombreDeEquipo.length<30){
      this.setState({nombreDeEquipo: nombreDeEquipo});
      this.flagNombre=true;

      }else{
        alert("El nombre del equipo es demasiado largo, porfavor ingrese un nombre mas corto");
        this.flagNombre=false;
      }
    } 

    handleSave(event) {
      
      if(this.state.nombreDeEquipo===""){
        alert("Por favor ingrese el nombre del equipo")   
        return false;
      }
      if(this.flagNombre===false){
        alert("No se puede almacenar un nombre tan largo");
        return false;
      }
      let token= sessionStorage.Token;
      let url= 'http://localhost:8090/savenewteam?name=';
      let teamName=this.state.nombreDeEquipo;
      let username=sessionStorage.Nombre;
      url+=encodeURIComponent(teamName);
      url+="&username="+username; 
      fetch(url,{method: 'POST', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
          }}).then(res => res.text())
          .then((res) => {
            res="res";
          },
           (error) => {
             alert("Upss hubo un error al guardar, intente de nuevo");
             console.log(error);
           });
           window.location="/equipos";


    
    }
    
     
    
    onChangeData(i,e){ 
      if(i===0){
        this.setState({
          personaSeleccionada:e.value
        });
        this.setState({personaSeleccionada:e.value});
      }
    }
    
  render() {
    return(
      <div>
      <NavBar></NavBar>
      <BackButton volver="/equipos"></BackButton>
        
      <div className="paper">
          <Paper>
      <h1 styles="margin-right:3em;">Nuevo Equipo</h1>
        <Paper>
        <TextField
        id="outlined-name"
        label="Nombre del equipo"
        onChange={this.onChange.bind(this)}
        variant="outlined"
      />
<br></br>
        <Button variant="contained" className="save"  onClick={this.handleSave.bind(this)}>
           Crear equipo
        </Button>
        </Paper>
     </Paper>
      </div>
</div>
    )
  }
}