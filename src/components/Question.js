import React, {Component} from 'react';
import './Question.css'
import TextField from '@material-ui/core/TextField';
import InputQuestion from './InputQuestion/InputQuestion';
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';

const contador=0; 
export default class Question extends Component {
  constructor(props){
    super(props);
    this.state = {
      input :"",
      respuesta:'',
      opciones:Array(1).fill(null),
      indexOpcion: 0
    };
    props=this.state.input;

    
  }
  _remove(){
    if(this.props.onRemove )
        this.props.onRemove();
        
  }
  onChangeinput(e){
    let input= e.target.input;
    this.setState({
      input:input
    });
    this.props.input=input;
  }
  onChange(e){
    let opciones = this.state.opciones.slice();
    let respuesta  = e.target.respuesta;
    opciones.push(respuesta);
    this.setState({
      respuesta:respuesta,
      opciones:opciones
    });
    this.props.respuesta[this.props.index][contador]=opciones;
    this.props.indexOpcion[this.props.index].push(this.state.indexOpcion);
    // eslint-disable-next-line
    contador++;
  }

  render() {
    return (
      <div className="">

        <Paper> 
      
       
            <Button color="secondary" className="remove" onClick={this._remove.bind(this)}>
            <CloseIcon className='icon-class' />
            </Button>
            <form  >
            <TextField
                id="outlined-full-width"
                label="Ingrese su pregunta: "
                fullWidth
                pregunta={this.props.pregunta}
                onChange={this.props.onChange}
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </form>
            <InputQuestion index={this.props.index} indexOpcion={this.state.indexOpcion} respuesta={this.props.respuesta} onChange={this.onChange.bind(this)} onClick={this.props.onClick}/>
            
            </Paper>
    </div> 
    )
  }
}


