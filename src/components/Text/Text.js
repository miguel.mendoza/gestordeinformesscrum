import React, { Component } from 'react';
import './Text.css'
import TextField from '@material-ui/core/TextField';


export default class Text extends Component {
 
  
  render() {
    return(
      <div>
        <TextField
            id="0"
            label={this.props.pregunta}
            style={{ margin: 4 }}
            fullWidth
            margin="normal"
            variant="outlined"
            onChange={this.props.onChange}
            respuesta={this.props.respuesta}
            name= {this.props.respuesta}
            InputLabelProps={{
              shrink: true,
            }}
          />
      </div>
    )
  }
}