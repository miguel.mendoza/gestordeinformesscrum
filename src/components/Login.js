import React, { Component } from 'react';
import './Login.css';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

export default class Login extends Component{
constructor(props){
  super(props);
  this.state = {
    user:"",
    password:"",
    showPassword: false,
  };
  this.handleChangePass=this.handleChangePass.bind(this);
  this.handleChangeUser=this.handleChangeUser.bind(this);
  this.handleClickShowPassword=this.handleClickShowPassword.bind(this);
  this.login=this.login.bind(this);
}
  
  login() {
    var myInit = { method: 'GET'};
    let myToken = {method: 'POST'};
    let url= "http://localhost:8090/log?userData=";
    url+=this.state.user;
    url+=","; 
    url+=this.state.password;
    let urlToken = "http://localhost:8090/logedUser?userData=";
    urlToken+=this.state.user;
    urlToken+=","; 
    urlToken+=this.state.password;
   
    fetch(url,myInit).then(res => res.text())
    .then((data) => { 
      if(data==="/"){
        alert("Nombre de usuario y/o contraseña incorrectos");
      }else{
        sessionStorage.setItem('Nombre', this.state.user);
        fetch(urlToken,myToken).then(res=>res.text())
        .then((dat)=>{
          sessionStorage.setItem('Token', dat);
        });
        window.location=data;  
      }
    },
      (error) => {
        this.setState({
          error
        });
      }
    )
  }

 handleChangeUser(event){
  
  let user=event.target.value;
  if(user.length<120){
    this.setState({
        user: user
      });
  }
    else{
      alert("El nombre de usuario es demasiado largo, excede el numero de caracteres");
    }
 }
  handleChangePass(event){
  let password =this.state.password;
  password=event.target.value;
    if(password.length<120){
      this.setState({
          password: password
      });
    }
    else{
      alert("La constraseña es demasiado largo, excede el numero de caracteres");
      }
  }

  handleClickShowPassword(e){
     let showPassword=this.state.showPassword;
     showPassword=!showPassword;
      this.setState({
        showPassword: showPassword
      });
  }
  showIcon(){
    if (this.state.showPassword ) {
      return <Visibility />;
         }
        return <VisibilityOff />;
  }
  render(){

    return (
      <div className="App">
      <br></br>     
            <div className="container">   
            <img className='logo'src="https://cdn.onlinewebfonts.com/svg/img_258083.png" alt="logo" />
                <div className={'margin'}>
                <Grid container spacing={1} alignItems="flex-end">
                  <Grid item>
                        <TextField  id="input-with-icon-grid" className={"input"} label="Username" onChange={this.handleChangeUser}/>
                    </Grid>
                </Grid>
               </div>
          <FormControl className={"margin"}>
            <InputLabel htmlFor="password">Password</InputLabel>

            <Input
              id="password"
              type={this.state.showPassword ? 'text' : 'password'}
              onChange={this.handleChangePass}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton aria-label="Toggle password visibility"  onClick={this.handleClickShowPassword}>
                    {this.showIcon()}
                  </IconButton>
                </InputAdornment>
              }
              />
          </FormControl>
          <br></br>
              <Button variant="contained" color="primary" className={'button'} onClick={this.login}>Login</Button>
          </div>
        
        </div>
      );
    }
  
}

