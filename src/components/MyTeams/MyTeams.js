import React, { Component } from 'react';
import './MyTeams.css';
import TeamSelect from '../TeamSelect/TeamSelect';
import { Paper, Typography, Button } from '@material-ui/core';
import BackButton from '../backButton/backButton';
import NavBar from '../NavBar';

export default class MyTeams extends Component {
  constructor(props){
    super(props);
    this.state={
      id:Array(0),
      nombresDelEquipos:Array(0),
      equipoSeleccionado:"",
      miembrosDelEquipo:Array(0),
      idMiembrosDelEquipo:Array(0),
      listaFinalDelEquipo:Array(0),
      todasLasPersonas:Array(0),
      idDeTdasLasPersonas:Array(0),
      nuevoMiembro:"",
      sinEquipo:Array(0),
      idSinEquipo:Array(0)
    };
    this.sinEquipo=this.sinEquipo;
    this.sinEquipo=[];
    this.idSinEquipo=this.idSinEquipo;
    this.idSinEquipo=[];
    
  }
  componentDidMount(){
    this.loged();
    this.recuperarGrupos();
    this.recuperarTodasLasPersonas();
    this.generarListaGeneral();

  }
  recuperarGrupos(){
    let token= sessionStorage.Token;
    let username= sessionStorage.Nombre;
    let url= 'http://localhost:8090/getAllTeams?username='+username;
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((gruposRecuperados) => {
          for(let i=0;i<JSON.parse(gruposRecuperados).length; i++){
            let id =this.state.id.slice();
            id.push(JSON.parse(gruposRecuperados)[i].id);
            let nombreDelEquipo =this.state.nombresDelEquipos.slice();
            nombreDelEquipo.push(JSON.parse(gruposRecuperados)[i].name);
            this.setState({ 
              id: id ,
              nombresDelEquipos: nombreDelEquipo
            });
          }
  
      },
        (error) => {
          console.log(error);
        });
  }
  recuperarTodasLasPersonas(){
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/getAllPerson';
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((personasRecuperadas) => {
         let personas= Array(0);
         let idPersonas= Array(0);
        for(let i=0;i<JSON.parse(personasRecuperadas).length; i++){
          idPersonas.push(JSON.parse(personasRecuperadas)[i].id);
          personas.push(JSON.parse(personasRecuperadas)[i].name+" "+JSON.parse(personasRecuperadas)[i].lastName);
          this.setState({ 
            idDeTdasLasPersonas: idPersonas ,
            todasLasPersonas: personas
          });
        }
      },
        (error) => {
          console.log(error);
        });
  }
   
  loged() {
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/loged?';
    let name= sessionStorage.Nombre;
    if(name===undefined){
      url+="username=null";  
    }
    else{
    url+="username="+name; 
    } 
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
    .then((data) => {
      if(data==="true"){
          return true;
      }else{  
        window.location="/";  
      }
    },
    (error) => {
      console.log(error);
    });
  }
  onChangeData(i,e){ 
    if(i===1){
      var equipoSeleccionado=this.state.equipoSeleccionado; 
      equipoSeleccionado=e.value;
      this.setState({equipoSeleccionado:equipoSeleccionado});
      this.recuperarMiembrosDelEquipo(equipoSeleccionado);     
    }
    if(i===2){
      var nuevoMiembro=this.state.nuevoMiembro; 
      nuevoMiembro=e.value;
      this.setState({nuevoMiembro:nuevoMiembro});
    }
  }  
  recuperarMiembrosDelEquipo(equipoSeleccionado){
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/getAllPersons?id=';
    url=url+equipoSeleccionado;
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((personasRecuperadas) => {
         let miembrosDelEquipo= Array(0);
         let idMiembrosDelEquipo= Array(0);
         for(let i=0;i<JSON.parse(personasRecuperadas).length; i++){
           idMiembrosDelEquipo.push(JSON.parse(personasRecuperadas)[i].id);
           miembrosDelEquipo.push(JSON.parse(personasRecuperadas)[i].name+" "+JSON.parse(personasRecuperadas)[i].lastName);
            this.setState({ 
              idMiembrosDelEquipo: idMiembrosDelEquipo,
              miembrosDelEquipo: miembrosDelEquipo
             });
            }
            if(miembrosDelEquipo.length===0){
            this.setState({
              miembrosDelEquipo:[],
              idMiembrosDelEquipo:[]

            })
          }
            
            
      },
        (error) => {
          console.log(error);
        });
  } 
  generarListaEquipo(){
    let i=-1;
    let listaEquipo;
    if(this.state.miembrosDelEquipo===[]){
      listaEquipo=<>hoola</>
    
    }else{
      listaEquipo= this.state.miembrosDelEquipo.map((miembroDelEquipo) =>
      <>
     <tr>
      <th>
        <li key={i++}>
          {miembroDelEquipo}
        </li>      
      </th>  
         <th>
       <Button className="delete" onClick={this.eliminar.bind(this,this.state.idMiembrosDelEquipo[i])}>Sacar del equipo</Button>
      </th>    
      </tr> 
    </> 
    )  

   
  }
    
  return <table>
    {listaEquipo}

  </table> 
  }
  
  generarListaGeneral(){
    let nombres=this.state.todasLasPersonas.slice();

    let miembros=this.state.miembrosDelEquipo.slice();
    let result;
    let result2;
    let ids=this.state.idDeTdasLasPersonas.slice();
    let idMiembros=this.state.idMiembrosDelEquipo;
    result=nombres.filter(miembro=> !miembros.includes(miembro));
    result2=(ids.filter(id=> !idMiembros.includes(id)));
    this.sinEquipo=result;
    this.idSinEquipo=result2;
    this.sinEquipo=result;
    this.idSinEquipo=result2;
    let i=-1;
    let lista= this.sinEquipo.map(miembroDelEquipo =>
    <> 
    <tr>
      <th>
        <li key={i++}>
          {miembroDelEquipo}
        </li>      
      </th>
      <th>
       <Button className="save" onClick={this.agregar.bind(this,this.idSinEquipo[i])}>Agregar al equipo</Button>
      </th>
    </tr> 
   </>   
  
   )
   return <table>{lista}</table>;
 }
 agregar(id,e){
   console.log(id)
   let token= sessionStorage.Token;
   let url="http://localhost:8090/setTeamAPerson?id=";
   url+=id+"&teamId="+this.state.equipoSeleccionado;
   if(this.state.equipoSeleccionado===""){
     alert("Por favor seleccione un equipo a donde añadir al empleado");
   }
   else{
     fetch(url,{method: 'POST', credentials: 'same-origin' ,
     headers: {
       "Authorization": token,
       "Access-Control-Allow-Origin": '*', 
      }});
      window.location="/editarEquipo";
    }
    }
  eliminar(id,e){
    console.log(id)
   let token= sessionStorage.Token;
   let url="http://localhost:8090/deleteaTeamAPerson?id=";
   url+=id+"&teamId="+this.state.equipoSeleccionado;
   fetch(url,{method: 'POST', credentials: 'same-origin' ,
   headers: {
      "Authorization": token,
      "Access-Control-Allow-Origin": '*', 
    }});
    window.location="/editarEquipo";
   }
  
  render() {
    return(
      <div>
         <NavBar></NavBar>
      <BackButton volver="/equipos"></BackButton>
         <div className="paper">
   <Paper className="date">
     <TeamSelect ids={this.state.id} nombresDeEquipos={this.state.nombresDelEquipos} onChange={this.onChangeData.bind(this,1)} ></TeamSelect>  
   </Paper>
  <div>
  </div> 
  <Paper> 
  <Typography variant="h5" component="h3">
    {this.state.equipoSeleccionado!=="" &&<> Empleados en el equipo</>}
    </Typography> 
   {this.generarListaEquipo()}
  </Paper>
   <br></br>
   <Paper>
   <Typography variant="h5" component="h3">
    Empleados 
    </Typography> 
   {this.generarListaGeneral()}
  </Paper>
  
</div>
      </div>
    )
  }
}