import React, { Component } from 'react'
import './viewForm.css'
import { Paper } from '@material-ui/core';
import BackButton from '../backButton/backButton';
import NavBar from '../NavBar';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import PersonSelect from '../PersonSelect/PersonSelect';
import DateSelect from '../DateSelect/DateSelect';

import { number } from 'prop-types';

export default class ViewForm extends Component {
        constructor(props) {
        super(props);
        this.state = {
          fechaSeleccionada:"",
          personaSeleccionada:number,
          fecha:Array(0),
          pregunta: Array(0),
          input: Array(0),
          respuesta:Array(0),  
          idPersonas: Array(0),
          personas:Array(0),
          id: Array(0) ,
          nombreDelEquipo: Array(0),
          topico:Array(0)   
        };
      }
      loged() {
        let token= sessionStorage.Token;
        let url= 'http://localhost:8090/loged?';
        let name= sessionStorage.Nombre;
        if(name===undefined){
          url+="username=null";  
        }
        else{
        url+="username="+name; 
        } 
        fetch(url,{method: 'GET', credentials: 'same-origin' ,
          headers: {
             "Authorization": token,
             "Access-Control-Allow-Origin": '*', 
           }}).then(res => res.text())
        .then((data) => {
          if(data==="true"){
              return true;
          }else{  
            window.location="/";  
          }
        },
        (error) => {
          console.log(error);
        });
      }
      componentDidMount(){
        this.loged();
        let personaSeleccionada= 1;
        this.setState({personaSeleccionada:personaSeleccionada});
        this.recuperarPersonas();

      }

     
      cargarFechas(personId){
        let token= sessionStorage.Token;
   
        let url= 'http://localhost:8090/loadAnswersbyPersonid?personId=';

        url=url+personId; 
        let fecha= Array(0);
        fetch(url,{method: 'GET', credentials: 'same-origin' ,
          headers: {
             "Authorization": token,
             "Access-Control-Allow-Origin": '*', 
           }}).then(res => res.text())
        .then((data) => {
          for(let i=0;i<JSON.parse(data).length; i++){
            fecha.push(JSON.parse(data)[i].createDate);
          }
          // console.log(fecha);
          this.setState({
              fecha:fecha,
            });
            // eslint-disable-next-line
            if(this.state.fecha.length==0){
              alert("Este empleado no tiene formularios llenados hasta ahora");
            }
        },
        (error) => {
          console.log(error);
        });
      }

      onChangeData(i,e){ 
        if(i===0){
          this.setState({
            personaSeleccionada:e.value
          });
          this.vaciarRespuestas();
          this.cargarFechas(e.value);
          this.setState({personaSeleccionada:e.value});

        }
        if(i===1){
            this.cargarRespuestas(this.state.personaSeleccionada,this.state.fecha[e.value]);

        }
      }
      vaciarRespuestas(){
        let pregunta= Array(0);
        let respuesta= Array(0);
        this.setState({
          pregunta:pregunta,
          respuesta:respuesta,
          fechaSeleccionada:" ",
        })
      }
      cargarRespuestas(idPersona,fechaSeleccionad){
        let token= sessionStorage.Token;
   
        let url= 'http://localhost:8090/loadAnswersbyPersonid?personId=';

        url=url+idPersona; 
        let pregunta= Array(0);
        let respuesta= Array(0);
        fetch(url,{method: 'GET', credentials: 'same-origin' ,
          headers: {
             "Authorization": token,
             "Access-Control-Allow-Origin": '*', 
           }}).then(res => res.text())
        .then((data) => {
          let v=0
       for(let j=0;j<JSON.parse(data).length;j++){
       
           if(JSON.parse(data)[j].createDate===fechaSeleccionad){
            for(v;v<JSON.parse(data)[j].preO3Questions.length; v++){
              pregunta.push(JSON.parse(data)[j].preO3Questions[v]);
              respuesta.push(JSON.parse(data)[j].answer[v]);
            }
            for(let i=0;i<JSON.parse(data)[j].form.questions.length; i++){
              for(let x=0;x<JSON.parse(data)[j].questionIndex.length; x++){
              // eslint-disable-next-line
                if(JSON.parse(data)[j].questionIndex[x]==i){
                  pregunta.push(JSON.parse(data)[j].form.questions[i]);
                  respuesta.push(JSON.parse(data)[j].answer[v]);
                  v++;
                  
                }
              }
              
            }
            pregunta.push(JSON.parse(data)[j].form.questions[JSON.parse(data)[j].form.questions.length-3]);
            respuesta.push(JSON.parse(data)[j].answer[JSON.parse(data)[j].answer.length-3]);
            pregunta.push(JSON.parse(data)[j].form.questions[JSON.parse(data)[j].form.questions.length-2]);
            respuesta.push(JSON.parse(data)[j].answer[JSON.parse(data)[j].answer.length-2]);
            pregunta.push(JSON.parse(data)[j].form.questions[JSON.parse(data)[j].form.questions.length-1]);
            respuesta.push(JSON.parse(data)[j].answer[JSON.parse(data)[j].answer.length-1]);
            let topico=Array(0);
            for(let i=0;i<JSON.parse(data)[j].topicChecklist.length; i++){
              topico.push(JSON.parse(data)[j].topicChecklist[i]);
            }
            this.setState({
              pregunta:pregunta,
              respuesta:respuesta,
              topico:topico
            });
          }
                 
       }
      
        },
        (error) => {
          console.log(error);
        });
     
     
      }

      
      recuperarPersonas(){
        let token= sessionStorage.Token;
        let url= 'http://localhost:8090/getAllPerson';
        fetch(url,{method: 'GET', credentials: 'same-origin' ,
          headers: {
             "Authorization": token,
             "Access-Control-Allow-Origin": '*', 
           }}).then(res => res.text())
           .then((personasRecuperadas) => {
             let personas= Array(0);
             let idPersonas= Array(0);
            for(let i=0;i<JSON.parse(personasRecuperadas).length; i++){
              idPersonas.push(JSON.parse(personasRecuperadas)[i].id);
              personas.push(JSON.parse(personasRecuperadas)[i].name+" "+JSON.parse(personasRecuperadas)[i].lastName);
              this.setState({ 
                idPersonas: idPersonas ,
                personas: personas
              });
            }
          },
            (error) => {
              console.log(error);
            });
      }   
     crearListaDeRespuestas(){
       let i=-1;
       console.log(this.state.respuesta)
       let lista = this.state.pregunta.map(pregunta=>
      <>
        {i===-1 && <> <Typography  variant="h5" component="h2">
        <strong> Preguntas Pre 3O</strong>
            </Typography>
            <Typography variant="body2" component="p">
             Estas preguntas tratan sobre:<br></br>{ this.state.topico.map(topico=><><li key={i+"topico"}>
               <strong>{topico} </strong>
               </li>
               </>
               )} 
          </Typography>
          </>
        }
         {i===2 &&  <Typography variant="h5" component="h2">
         <strong>Preguntas Seleccionadas</strong> 
          </Typography>
           } 
         {i===5 &&  <Typography variant="h5" component="h2">
         <strong>Acciones para la proxima entrevista</strong>
          </Typography>
          }
        <Card key={i++}>
          <CardContent>
            <Typography variant="h5" component="h2">
            {pregunta} 
           </Typography>
           <Typography variant="body2" component="p">
              {this.state.respuesta[i]} 
            </Typography>
          </CardContent>
       </Card> 
      </>
       )
        return lista;
     }

     recuperarGrupos(){
      let token= sessionStorage.Token;
      let username= sessionStorage.Nombre;
      let url= 'http://localhost:8090/getAllTeams?username='+username;
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
         .then((gruposRecuperados) => {
            for(let i=0;i<JSON.parse(gruposRecuperados).length; i++){
              let id =this.state.id.slice();
              id.push(JSON.parse(gruposRecuperados)[i].id);
              let nombreDelEquipo =this.state.nombreDelEquipo.slice();
              nombreDelEquipo.push(JSON.parse(gruposRecuperados)[i].name);
              this.setState({ 
                id: id ,
                nombreDelEquipo: nombreDelEquipo
              });
            }
        },
          (error) => {
            console.log(error);
          });
    }
   
    render() {
      return(
      <div >
        
          <NavBar></NavBar>
            <BackButton volver="/index"></BackButton>
            <Paper className='paper'>
                <div className='BasicQuestions'>
                    <Card>
                      <Typography variant="h5" component="h2">
                        <strong> Nombre del entrevistado:</strong> 
                        <PersonSelect ids={this.state.idPersonas} personas={this.state.personas} onChange={this.onChangeData.bind(this,0)} ></PersonSelect> 
                      </Typography>
                      <Typography variant="h5" component="h2">
                        <strong> Fecha de la entrevista: </strong>
                        <DateSelect fecha={this.state.fecha} onChange={this.onChangeData.bind(this,1)} ></DateSelect>
                      </Typography>
                    </Card>
                  <div> 
                
                    {this.crearListaDeRespuestas()}
                 </div> 
               </div>           
             </Paper>
            
      </div>
      )
    }
  }

