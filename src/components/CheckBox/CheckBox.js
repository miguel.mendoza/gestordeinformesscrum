import React, { Component } from 'react'
import '../CheckBox/CheckBox.css'
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {Checkbox} from '@material-ui/core';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';



export default class CheckBox extends Component {
  constructor(props){
    super(props);
    this.state = {
      data : [
        {
          
        }
      ],
      opciones:this.props.opciones,
      input: 'check',
      index: this.props.index,
     
    };
    this.handleChange = this.handleChange.bind(this);

  }
 
 
  handleChange(e,i){
    let resp= e.target;
    this.props.onChange(resp);
  }
  generarLista(){
    let i=0;
     let listaDePreguntas=  this.state.opciones.map(opcion =>
             <li key={i++}>
                <FormControlLabel
             control={<Checkbox   value={opcion} />}
             label={opcion}
          />
               
             </li>
      )
       return listaDePreguntas
  }
          
 
  render() {
    return(
       <div className='root'>
      <FormControl component="fieldset" className='formControl'>
        <FormLabel component="legend">{this.props.pregunta} </FormLabel>
        <FormGroup onChange={this.handleChange}>
         {this.generarLista()}
        </FormGroup>
      </FormControl>

      </div>
    )
  }
}