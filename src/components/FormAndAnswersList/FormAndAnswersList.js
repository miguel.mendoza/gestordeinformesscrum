import React, { Component } from 'react';
import './FormAndAnswersList.css';

import Paper from '@material-ui/core/Paper';
import TeamSelect from '../TeamSelect/TeamSelect';


export default class FormAndAnswersList extends Component {
  constructor(props){
    super(props);
    this.state={
      personas: Array(0),
      idPersonas: Array(0),
      selectedPerson:'',
      id: Array(0),
      nombreDelEquipo: Array(0),
      idFormulario:Array(0),
      idAnswer:Array(0),
      nombreEntrevistado:Array(0)

    };
  }
  
  recuperarDatosDeTodasLasRespuestas(idForms){
    // let token= sessionStorage.Token;
    // let url= 'http://localhost:8090/loadanswersbyformid?formId=';
    console.log(idForms);
    return idForms;
    // for(let i=0; i<=this.state.idForm.length;i++){
    // // console.log(url+" "+this.state.idForm+ " "+ i);
    //   url=url+this.state.idForm[0];
    //   fetch(url,{method: 'GET', credentials: 'same-origin' ,
    //   headers: {
    //     "Authorization": token,
    //     "Access-Control-Allow-Origin": '*', 
    //   }}).then(res => res.text())
    //   .then((answRecuperado) => {
    //     let idAnsw=this.state.idAnswer.slice();
    //     let nombreEntrevistado=this.state.nombreEntrevistado.slice();
    //     for(let i; i<JSON.parse(answRecuperado).length;i++){
    //       idAnsw.push(JSON.parse(answRecuperado)[i].id);
    //       nombreEntrevistado.push(JSON.parse(answRecuperado)[i].person.name +JSON.parse(answRecuperado)[i].person.lastName);
    //     }
    //     this.setState({ 
    //       idAnsw:idAnsw,
    //       nombreEntrevistado:nombreEntrevistado
    //     });
    //   },
    //   (error) => {
    //     console.log(error);
    //   });
    //   url= 'http://localhost:8090/loadanswersbyformid?formId=';
    // }
  }
  recuperarFormularios(){
    let token= sessionStorage.Token;
    let nombre=sessionStorage.Nombre;
    let url= 'http://localhost:8090/loadForms?username=';
      url=url+nombre;
        fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((formulariosRecuperadas) => {
        let idFormulario=Array(0);
        for(let i=0;i<JSON.parse(formulariosRecuperadas).length; i++){
          idFormulario.push(JSON.parse(formulariosRecuperadas)[i].id);
          this.setState({idFormulario:idFormulario});
        }
      },
        (error) => {
          console.log(error);
        });
        
  }
  
  recuperarPersonas(){
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/getAllPersons?id=1';
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((personasRecuperadas) => {
         let personas= Array(0);
         let idPersonas= Array(0);
        for(let i=0;i<JSON.parse(personasRecuperadas).length; i++){
          idPersonas.push(JSON.parse(personasRecuperadas)[i].id);
          personas.push(JSON.parse(personasRecuperadas)[i].name+" "+JSON.parse(personasRecuperadas)[i].lastName);
          this.setState({ 
            idPersonas: idPersonas ,
            personas: personas
          });
        }
      },
        (error) => {
          console.log(error);
        });
  }
  recuperarGrupos(){
    let token= sessionStorage.Token;
    let username= sessionStorage.Nombre;
    let url= 'http://localhost:8090/getAllTeams?username='+username;
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((gruposRecuperados) => {
          for(let i=0;i<JSON.parse(gruposRecuperados).length; i++){
            let id =this.state.id.slice();
            id.push(JSON.parse(gruposRecuperados)[i].id);
            let nombreDelEquipo =this.state.nombreDelEquipo.slice();
            nombreDelEquipo.push(JSON.parse(gruposRecuperados)[i].name);
            this.setState({ 
              id: id ,
              nombreDelEquipo: nombreDelEquipo
            });
          }
      },
        (error) => {
          console.log(error);
        });
  }
  
  onChangeData(i,e){ 
    this.setState({
        selectedPerson:e.value
    });
    this.recuperarPersonas();
  } 
  componentDidMount(){
    this.recuperarFormularios();
   this.recuperarGrupos();

  }
  
   
  render() {
    return(
      <div>
      
         <Paper>
           <TeamSelect ids={this.state.id} nombresDeEquipos={this.state.nombreDelEquipo} onChange={this.onChangeData.bind(this,1)} ></TeamSelect>  
           {/* <PersonSelect ids={this.state.idPersonas} personas={this.state.personas} onChange={this.onChangeData.bind(this,0)} ></PersonSelect>  */}
         </Paper>

 
      </div>
    )
  }
}