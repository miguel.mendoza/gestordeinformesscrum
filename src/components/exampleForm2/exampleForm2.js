import React, { Component } from 'react'
import QuestionTemplate from '../QuestionTemplate/QuestionTemplate';
import './exampleForm2.css';
import TipsForaAnEffectiveOneOnOne from '../TipsForaAnEffectiveOneOnOne/TipsForaAnEffectiveOneOnOne';
import BasicQuestions from '../BasicQuestions/BasicQuestions';
import { Paper, Button } from '@material-ui/core';
import BackButton from '../backButton/backButton';
import NavBar from '../NavBar';
export default class ExampleForm extends Component {
    constructor(props) {
      super(props);
      this.state = {
        datosPersonales: ['','',''],
        pregunta: ['¿En que proyecto esta trabajando el empleado? ','¿Qué desafío ha encontrado el entrevistado esta semana/mes?','¿Qué ha aprendido el entrevistado esta semana/mes ?','¿Qué progreso ha hecho el empleado hacia sus metas esta semana/mes?','Enlista algunas de las frustraciones o desafios que ha mencionado el empleado','Enlista los elementos que el empleado compartió donde podria necesitar ayuda','¿El empleado expresó algún interés en futuros roles o posiciones?','¿Que acciones que se le dará al empleado para el próximo One-On-One?','¿Se encuentra satisfecho con el trato que recibe como empleado?','¿Que le gustaria que haya para servirse en la oficina'],
        input: ['text','text','text','text','textArea','textArea','text','text','radio','checkbox'],
        opciones:['','','','','','','','',['si','no'],['mas cafe',' mas fruta',' empanadas']],
        respuesta:Array(10).fill(''),
        temporal:Array(0)
      };
      this.onChange = this.onChange.bind(this);
      this.handleSave = this.handleSave.bind(this);
    }
    loged() {
      let token= sessionStorage.Token;
      console.log(token);
      let url= 'http://localhost:8090/loged?';
      let name= sessionStorage.Nombre;
      if(name===undefined){
        url+="username=null";  
      }
      else{
      url+="username="+name; 
      } 
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
      .then((data) => {
        if(data==="true"){
            return true;
        }else{  
          window.location="/";  
        }
      },
      (error) => {
        
      });
    }
    onChange(i,e) { 
      var respuesta=this.state.respuesta.slice() ;
      respuesta[i] = e.target.value; 
      this.setState({respuesta: respuesta});
    } 
    onClick(i,e){
      let respuesta=this.state.respuesta.slice() ;
      respuesta[i] = e.target.value; 
      this.setState({respuesta: respuesta,}); 
    }

    onClickCheck(i,e){
      let opciones=this.state.opciones.slice();
      let respuesta=this.state.respuesta.slice();
      let temp=this.state.temporal.slice();
      let tempo2=Array(0);
      if(e.checked){
        for(let j=0;j<opciones[i].length;j++){
            if(opciones[i][j]===e.value){
              temp.push(opciones[i][j]);
              respuesta[i]=temp;
              this.setState({temporal:temp});
            }
        }
      }else {
          for(let j=0;j<opciones[i].length;j++){
          if(temp[j]!==e.value){
           tempo2.push(temp[j]);
           this.setState({temporal:tempo2});
          }
          respuesta[i]=tempo2;
        } 
      }
      let aux=Array(0).fill(null);
      for(let j=0;j<respuesta[i].length;j++){
        if(respuesta[i][j]!=null){
          aux.push(respuesta[i][j]);
        }    
      }
      respuesta[i]=aux  ;
      this.setState({respuesta:respuesta});
    }
           
    generarLista(){
      let i=-1;
      let listaDePreguntas= this.state.pregunta.map((pregunta) =>
        <li key={i++}>
          {this.state.input[i]==='text' &&  <QuestionTemplate index={i} onClick={this.onClick.bind(this, i)} onChange={this.onChange.bind(this, i)} pregunta={pregunta} opciones={this.state.opciones[i]} input={this.state.input[i]} respuesta={this.state.respuesta[i]}/>}
          {this.state.input[i]==='textArea' &&  <QuestionTemplate index={i} onClick={this.onClick.bind(this, i)} onChange={this.onChange.bind(this, i)} pregunta={pregunta} opciones={this.state.opciones[i]} input={this.state.input[i]} respuesta={this.state.respuesta[i]}/>}
          {this.state.input[i]==='checkbox' &&  <QuestionTemplate index={i} onChange={this.onClickCheck.bind(this,i)} pregunta={pregunta} opciones={this.state.opciones[i]} input={this.state.input[i]} respuesta={this.state.respuesta[i]}/>}
         {this.state.input[i]==='radio' &&  <QuestionTemplate index={i} onClick={this.onClick.bind(this, i)} onChange={this.onClickCheck.bind(this,i)} pregunta={pregunta} opciones={this.state.opciones[i]} input={this.state.input[i]} respuesta={this.state.respuesta[i]}/>}
         </li>      
      )
      return listaDePreguntas
    }

    getResponse(datosPersonales){
      var newDatosPersonales=this.state.datosPersonales.slice() ;
      newDatosPersonales[0]= datosPersonales[0];
      newDatosPersonales[1]= datosPersonales[1];
      newDatosPersonales[2]= datosPersonales[2];
      this.setState({datosPersonales: newDatosPersonales});
    }

    handleSave(event) {
      let list = {
        'formulario' :[]
      };
      list.formulario.push({
        "Nombre": this.state.datosPersonales[0],
        "Equipo": this.state.datosPersonales[1],
        "Fecha de la entrevista": this.state.datosPersonales[2],
      });
      for (var i = 0; i < this.state.pregunta.length; i++) {
        list.formulario.push({
          "Pregunta": this.state.pregunta[i],
          "Respuesta": this.state.respuesta[i],
        });
      };
      var json = JSON.stringify(list);
      console.log(this.state);   
      console.log(json);
      this.GenerarJSONFile(list.formulario)
    }
    GenerarJSONFile(formulario){
      var jsonObj = {
                    Formulario: formulario
                  };
      var fileText = new Blob([JSON.stringify(jsonObj)],{type:"application/json"});
      var textuRL = window.URL.createObjectURL(fileText);

      var downloadLink = document.createElement("a");
      downloadLink.download = "Respuestas_formulario.json";
      downloadLink.innerHTML = "";
      downloadLink.href = textuRL;

      document.body.appendChild(downloadLink);
      downloadLink.click();
    }

  render() {
    return(
      <div >
              {this.loged()}

        <NavBar></NavBar>
        <BackButton volver="/index"></BackButton>
        <TipsForaAnEffectiveOneOnOne/>
          <form>
          <Paper className='paper'>
            <BasicQuestions callback={this.getResponse.bind(this)}  datosPersonales={this.state.datosPersonales}/>
            <h4>Preguntas para el entrevistado</h4>
            {this.generarLista()}    
            <Button variant="contained"  onClick={this.handleSave}>
              Guardar
            </Button>      
          </Paper>
          </form>
      </div>
    )
  }
}