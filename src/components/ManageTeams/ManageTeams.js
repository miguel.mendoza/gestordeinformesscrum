import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import NavBar from '../NavBar';
import { CardContent, Typography } from '@material-ui/core';
import BackButton from '../backButton/backButton';

export default class ManageTeams extends Component {
  
  constructor(props){
      super(props);
      this.state={
        form:''
      };
    }
    componentDidMount(){
     this.loged();
    }
    
    loged() {
      let token= sessionStorage.Token;
      let url= 'http://localhost:8090/loged?';
      let name= sessionStorage.Nombre;
      if(name===undefined){
        url+="username=null";  
      }
      else{
      url+="username="+name; 
      } 
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
      .then((data) => {
        if(data==="true"){
            return true;
        }else{  
          window.location="/";  
        }
      },
      (error) => {
        
      });
    }
   
    render() {
      return (
        <div className="App_Format">
        <NavBar></NavBar>
        <BackButton volver={"/administrar"}></BackButton>

        <div className='cardContainer'>     
       <Card >
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant="h6" component="h2">
            Nuevo Equipo
            </Typography>
           
            </CardContent>
          <Button  className="button1" href='/nuevoEquipo' size="small" color="primary">
          <CardMedia
            component="img"
            image='https://image.flaticon.com/icons/svg/1006/1006515.svg'
            title="Nuevo Equipo" 
          />
          </Button>
  
        </CardActionArea>
      </Card>
      <Card >
         <CardActionArea>
            <CardContent>
            <Typography gutterBottom variant="h6" component="h2">
            Editar Equipo 
            </Typography>
            
          </CardContent>
          <Button  className="button1" href='/editarEquipo' size="small" color="primary">
          <CardMedia
          component="img"
          image='https://image.flaticon.com/icons/svg/1006/1006558.svg'
          title="Editar Equipo" 
          />
          </Button>
          </CardActionArea>
      </Card>
    
      </div>
        </div>
        
      )
    }
  }
  
  
  