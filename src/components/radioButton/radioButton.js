import React, { Component } from 'react'
import './radioButton.css'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
 

 
export default class RadioButton extends Component {
  constructor(props){
    super(props);
    this.state = {
      data : [
        {
          
        }
      ],
      respuesta: [],
      opciones:this.props.opciones,
      input: 'radio',
      index: this.props.index,
    };
  }
  generarLista(){
    let i=0;
    let listaDePreguntas=  this.state.opciones.map(opcion =>
            <li key={i++}>
              <FormControlLabel value={opcion} onClick={this.props.onClick} control={<Radio />} label={opcion}/>
            </li>
     )
       return listaDePreguntas
  }
    
  render() {
    return(
      <div>
      <FormControl component="fieldset" className='formControl'>
        <FormLabel component="legend">{this.props.pregunta}</FormLabel>
        <RadioGroup
          aria-label="opciones"
          value={this.props.respuesta}
        >
        {this.generarLista()}
        </RadioGroup>
      </FormControl>
      </div>
    )
  }
}

