import React, {Component} from 'react';
import Question from './Question';
import {Button, Paper} from '@material-ui/core';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import './NewForm.css';
import AddButton from './AddButton/AddButton';
import BackButton from './backButton/backButton';
import NavBar from './NavBar';

export default class NewForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      pregunta:Array(1).fill(null),
      respuesta:Array(1).fill(Array(1).fill(null)),
      input:Array(1).fill("text"),
      index:Array(1).fill(0), 
      indexOpcion:Array(0),
      data : [
        {
          index:''
        }
      ],
    };
    this.handleSave = this.handleSave.bind(this);
  }

  loged() {
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/loged?';
    let name= sessionStorage.Nombre;
    if(name===undefined){
      url+="username=null";  
    }
    else{
    url+="username="+name; 
    } 
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
    .then((data) => {
      if(data==="true"){
          return true;
      }else{  
        window.location="/";  
      }
    },
    (error) => {
      console.log(error);
    });
  }

  _remove(position){
     if( this.state.respuesta.length!==1){
       let { data } = this.state;
       let newData = [
         ...data.slice(0, position),
         ...data.slice(position + 1),
        ];
        this.setState({ data : newData });
        let respuesta =this.state.respuesta.slice();    
        let indexOpcion=this.state.indexOpcion.slice();
        let pregunta =this.state.pregunta.slice(); 
        let input =this.state.input.slice();    
        
        respuesta.splice(position,1);
        input.splice(position,1);
        pregunta.splice(position,1);
        
        this.setState({ respuesta : respuesta, indexOpcion:indexOpcion,pregunta:pregunta ,input:input});
        
      }else{
        alert("No puedes eliminar este elemento");
      }
  }
  _add(){
    let respuesta =this.state.respuesta.slice();    
    let indexOpcion=this.state.indexOpcion.slice();
    let pregunta =this.state.pregunta.slice(); 
    pregunta.push(Array(1).fill(null))
    respuesta.push(Array(1).fill(null));
    indexOpcion.push(Array(1).fill(0));

    this.setState({ respuesta : respuesta, indexOpcion:indexOpcion });

    let { data } = this.state;
    let newData = [
      ...data,
      {
        respuesta  : '',
        input : '',
      }
    ]
    this.setState({ data : newData });
  }

  onClickInput(i,e){
    let input=this.state.input.slice() ;
    input[i] = e.target.value; 
    this.setState({input: input,});
    
  }
  onChange(e,i){
    this.onChangePregunta(i,e);
  }
  onChangePregunta(e,i) { 
    let pregunta=this.state.pregunta.slice() ;
    pregunta[i] = e.target.value; 
    this.setState({pregunta: pregunta,});
  } 
 
  handleSave() {
    let indexOpcion=this.state.indexOpcion;
        for (let i = 0; i < this.state.respuesta.length; i++) {
            indexOpcion.push(this.state.respuesta[i].length);
          }
        
          this.setState({
            indexOpcion: indexOpcion
          });
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/loged?';
    let name= sessionStorage.Nombre;
    if(name===undefined){
      url+="username=null";  
    }
    else{
    url+="username="+name; 
    } 
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
    .then((data) => {
      if(data==="true"){
          return true;
      }else{  
        window.location="/";  
      }
    },
    (error) => {
      console.log(error);
    });
    
    this.guardarEnServer();
  }
  guardarEnServer(){
    let token= sessionStorage.Token;
    let preguntas = this.state.pregunta;
    for(let i=0;i<preguntas.length;i++){
      if(preguntas[i]===null){
        alert("Por favor revise que las preguntas han sido llenadas");
        return false
      }
    }
    let inputs = this.state.input;
    let opciones=this.state.respuesta;
    let username= sessionStorage.Nombre;
    let indexOpcion =this.state.indexOpcion;
    preguntas.push();
    preguntas.push();
    preguntas.push();
    inputs.push("text");
    inputs.push("text");
    inputs.push("textArea");
    opciones.push("");
    opciones.push("");
    opciones.push("");
    indexOpcion.push(0);
    indexOpcion.push(0);
    indexOpcion.push(0);
    let url= 'http://localhost:8090/saveform?questions='+encodeURIComponent(preguntas)+'&inputs='+inputs+
    '&options='+encodeURIComponent(opciones)+'&optionsNumber='+indexOpcion+'&username='+username;
    fetch(url,{method: 'POST', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }});
       window.location="/index"
  }
  
  GenerarJSONFile(template){
  var jsonObj = {
    template: template
  };
  var fileText = new Blob([JSON.stringify(jsonObj)],{type:"application/json"});
  var textuRL = window.URL.createObjectURL(fileText);
  var downloadLink = document.createElement("a");
  downloadLink.download = "template_formulario.json";
  downloadLink.innerHTML = "";
  downloadLink.href = textuRL;
  document.body.appendChild(downloadLink);
  downloadLink.click();
  }

  generarLista(){
    let i=-1;
    let listaDePreguntas=  this.state.data.map(index =>
       <> <li key={i++}>
         <div className="box">
          <Question  onChange={this.onChange.bind(this, i)} onClick={this.onClickInput.bind(this,i)} indexOpcion={this.state.indexOpcion}  respuesta={this.state.respuesta[i]} index={i}
           pregunta={this.state.pregunta} onRemove={ () => this._remove(i)} />
           </div>
        </li>
        <br></br></>
    )
    return listaDePreguntas
  }
  componentDidMount(){
          this.loged()
  }

  render(){
    return (
      <div>
          <NavBar></NavBar>
              <BackButton volver="/index"></BackButton>
                
              <div className="paper">
                  <Paper>
              <h1 styles="margin-right:3em;">Nueva Plantilla de Formulario</h1>
                <ul >
                {this.generarLista()}
                </ul>
                <div className="buttonBox">
                    <ButtonGroup
                      className='group'
                      variant="contained"
                      color="primary"
                      aria-label="Full-width contained primary button group"
                      >
                      <AddButton  onClick={this._add.bind(this)} >Añadir Pregunta</AddButton>
                      <Button className="save"  onClick={this.handleSave} >Guardar Plantilla</Button>
                    </ButtonGroup>
                </div>
                      </Paper>
              </div>
      </div>
    )
  }
}

