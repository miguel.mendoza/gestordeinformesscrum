import React, { Component } from 'react'
import './TextArea.css'
import TextField from '@material-ui/core/TextField';

export default class TextArea extends Component {
  
  
  render() {
    return(
      <div>
    <TextField
    id="0"
    label={this.props.pregunta}
    style={{ margin: 4 }}

    multiline
    fullWidth
    onChange={this.props.onChange}
    respuesta={this.props.respuesta}
    name= {this.props.respuesta}
    rows="4"
    variant="outlined"
    />
    </div>
  )
  }
}