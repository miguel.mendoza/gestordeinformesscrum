import React from 'react'
import './backButton.css'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
const button = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  
}));
const BackButton = (props) => (
  <div className="div">
      <Button href={props.volver} variant="contained" color="secondary" className={button.button}>
        Volver
      </Button>
    
  </div>
)

export default BackButton
