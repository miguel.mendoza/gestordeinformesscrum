import React, { Component } from 'react'
import QuestionTemplate from '../QuestionTemplate/QuestionTemplate';
import './exampleForm.css';
import TipsForaAnEffectiveOneOnOne from '../TipsForaAnEffectiveOneOnOne/TipsForaAnEffectiveOneOnOne';
import { Paper, Button, TextField, FormControl, FormLabel, FormGroup, FormControlLabel, Checkbox } from '@material-ui/core';
import BackButton from '../backButton/backButton';
import NavBar from '../NavBar';
import TeamSelect from '../TeamSelect/TeamSelect';
import PersonSelect from '../PersonSelect/PersonSelect';
import Typography from '@material-ui/core/Typography';



export default class ExampleForm extends Component {
    constructor(props) {
      super(props);
      this.state = {
        datosPersonales: ['','',''],
        preguntas: Array(0),
        inputs:Array(0),
        checkPregunta: Array(0),
        topicos:Array(0),
        temporal:Array(0),
        opciones:Array(0),
        numeroDeOpciones:Array(0),
        respuesta:Array(11).fill(null),
        selectedPerson:'',
        selectedTeam:'',
        id: Array(0),
        nombreDelEquipo: Array(0),
        personas: Array(0),
        idPersonas: Array(0),
        respuestasSeleccionadas:Array(0),
        preguntasPreO3: ['','',''],
        respuestaPre03: ['','','']
      };
      this.contador=this.contador;
      this.contador=0;
      this.onChange = this.onChange.bind(this);
      this.handleSave = this.handleSave.bind(this);
    }

    loged() {
      let token= sessionStorage.Token;
      let url= 'http://localhost:8090/loged?';
      let name= sessionStorage.Nombre;
      if(name===undefined){
        url+="username=null";  
      }
      else{
      url+="username="+name; 
      } 
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
      .then((data) => {
        if(data==="true"){
            return true;
        }else{  
          window.location="/";  
        }
      },
      (error) => {
        console.log(error);
      });
    }

    onChange(i,e) { 
      var respuesta=this.state.respuesta.slice() ;
      respuesta[i] = e.target.value; 
      this.setState({respuesta: respuesta});
    } 
    anadirPreguntaO3(i,e){
      let preguntasO3=this.state.preguntasPreO3.slice();
      preguntasO3[i]=e.target.value;
      this.setState({preguntasPreO3:preguntasO3});
    }
    anadirRespuestaPreO3(i,e){
      let respuestaPre03=this.state.respuestaPre03.slice();
     respuestaPre03[i]=e.target.value;
      this.setState({respuestaPre03:respuestaPre03});
    }
   recuperarFormulario(){
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/form?formId=0';
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((formRecuperado) => {
        let preguntas =this.state.preguntas.slice();
        let inputs =this.state.inputs.slice();
        let opciones =this.state.opciones.slice();
        let numeroDeOpciones=this.state.numeroDeOpciones.slice();
        let checkPregunta = this.state.checkPregunta.slice();
          for(let i=0;i<JSON.parse(formRecuperado).questions.length; i++){
            preguntas.push(JSON.parse(formRecuperado).questions[i]);
            checkPregunta.push(false);            
          }
          for(let i=0;i<JSON.parse(formRecuperado).inputs.length; i++){
            inputs.push(JSON.parse(formRecuperado).inputs[i]);
            
          }
          for(let i=0;i<JSON.parse(formRecuperado).options.length; i++){
            opciones.push(JSON.parse(formRecuperado).options[i]);
          
          }
          for(let i=0;i<JSON.parse(formRecuperado).optionsNumber.length; i++){
            numeroDeOpciones.push(JSON.parse(formRecuperado).optionsNumber[i]);
          }
           this.setState({ 
              preguntas:preguntas,
              inputs:inputs,
              opciones:opciones,
              numeroDeOpciones:numeroDeOpciones,
              checkPregunta:checkPregunta
              });
      },
        (error) => {
          console.log(error);
        });
   }
   handleChangeCheckBox(e){
      let respuestaSeleccionada=this.state.respuestasSeleccionadas.slice();
      let tempo2=Array(0);
     
      if(e.target.checked){
        // eslint-disable-next-line
        if(this.contador<3){
          respuestaSeleccionada.push(e.target.value);
          this.contador++;
          this.setState({
            respuestasSeleccionadas:respuestaSeleccionada
              });
            }
            else{
              e.target.checked=false;
            }
            
          }else {
            // eslint-disable-next-line
            for(let i=0;i<respuestaSeleccionada.length;i++){
              if(respuestaSeleccionada[i]!==e.target.value)
              {
                tempo2.push(respuestaSeleccionada[i]);
              } 
            }
            this.contador--;
            this.setState({respuestasSeleccionadas:tempo2});

          }
       
          this.deshabilitarCheckboxs();
         

            
  }
  deshabilitarCheckboxs(){ 
   let checkPregunta = this.state.checkPregunta.slice();
    if(this.contador===3){
      for(let x=0;x<this.state.checkPregunta.length;x++){
        checkPregunta[x]=true;
      }
      for(let j=0;j<=this.state.respuestasSeleccionadas.length;j++){
        for(let i=0;i<=this.state.preguntas.length;i++){
          // eslint-disable-next-line
           if(i==this.state.respuestasSeleccionadas[j]){
            
             checkPregunta[i]=false;
            }
        }
        
      } 
      // console.log(this.state.respuestasSeleccionadas);
    }else{
      for(let x=0;x<this.state.checkPregunta.length;x++){
        checkPregunta[x]=false;
      }
    }
    this.setState({checkPregunta:checkPregunta});
  }
          
    generarLista(){
      let i=-1;
      let listaDePreguntas= this.state.preguntas.map((pregunta) =>
          <li key={i++}>
        {i<this.state.preguntas.length-3 && <>
          <Checkbox
          disabled={this.state.checkPregunta[i]}
          onChange={this.handleChangeCheckBox.bind(this)}
          value={i}
         />Realicé esta pregunta durante la entrevista
          <QuestionTemplate  disabled={this.state.checkPregunta[i]}
          onChange={this.onChange.bind(this, i)} pregunta={pregunta} input={this.state.inputs[i]}/> 
          </>
         
        }
       </li>

        
      )
      return listaDePreguntas
    }
    generarLista2(){
      let i=-1;
      let listaDePreguntas= this.state.preguntas.map((pregunta) =>
          <li key={i++}>
        {i>=this.state.preguntas.length-3 && <QuestionTemplate  onChange={this.onChange.bind(this, i)} pregunta={pregunta} input={this.state.inputs[i]}/>
        }
       </li>

        
      )
      return listaDePreguntas
    }


    getResponse(datosPersonales){
      let newDatosPersonales=this.state.datosPersonales.slice() ;
      newDatosPersonales[0]= datosPersonales[0];
      newDatosPersonales[1]= datosPersonales[1];
      newDatosPersonales[2]= datosPersonales[2];
      this.setState({datosPersonales: newDatosPersonales});
    }

    componentDidMount(){
      this.loged()
      this.recuperarGrupos();
      this.recuperarFormulario();
    }
    
    recuperarPersonas(e){
      let token= sessionStorage.Token;
      let url= 'http://localhost:8090/getAllPersons?id='+e;
      
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
         .then((personasRecuperadas) => {
           let personas= Array(0);
           let idPersonas= Array(0);
          for(let i=0;i<JSON.parse(personasRecuperadas).length; i++){
            idPersonas.push(JSON.parse(personasRecuperadas)[i].id);
            personas.push(JSON.parse(personasRecuperadas)[i].name+" "+JSON.parse(personasRecuperadas)[i].lastName);
            this.setState({ 
              idPersonas: idPersonas ,
              personas: personas
            });
          }
        },
          (error) => {
            console.log(error);
          });
    }
    onChangeData(i,e){ 
      var datosPersonales=this.state.datosPersonales.slice() ; 
      if(i===0){
        for(i=0; i<this.state.idPersonas.length;i++){
          if(e.value===this.state.idPersonas[i]){
            this.setState({
              personaSeleccionada:e.value
            });    
                   
             datosPersonales[0]=this.state.personas[i];
             this.setState({
               datosPersonales: datosPersonales,
               selectedPerson:e.value
            });
           }  
         }
      }
      else
        if(i===1){
          for(i=0; i<this.state.id.length;i++){
            if(e.value===this.state.id[i]){
              datosPersonales[1]=this.state.nombreDelEquipo[i];
              this.setState({datosPersonales: datosPersonales,
                selectedTeam:e.value});
            }  
          }  
          this.recuperarPersonas(e.value);
        }
      else{
          datosPersonales[2] = e.target.value;  
          this.setState({datosPersonales: datosPersonales});
      }
      
    } 

    handleSave(event) {
      if(this.state.datosPersonales[1]===""){
        alert("Por favor seleccione el equipo del entrevistado")   
        return false;
      }
      if(this.state.datosPersonales[0]===""){
        alert("Por favor seleccione al entrevistado")   
        return false;
      }
      if(this.state.datosPersonales[2]===""){
        alert("Por favor seleccione la fecha de la entrevista")   
        return false;
      }
      let token= sessionStorage.Token;
      let url= 'http://localhost:8090/saveAnswer?formId=';
      let formId=0;
      let respuesta=Array(0);
      let preO3Questions=this.state.preguntasPreO3;
      for(let i=0;i<preO3Questions.length;i++){
        if(preO3Questions[i]===""){
          alert("Revise que las preguntas de las notas Pre One-on-One no esten en blanco e intente de nuevo"); 
          return false;
        }
      }
      for(let i=0;i<3;i++){
        if(this.state.respuestaPre03[i]===""||this.state.respuestasSeleccionadas[i]===""){
          alert("Revise que las respuestas a las preguntas de las notas Pre One-on-One no esten en blanco e intente de nuevo");
          return false;
        }
        respuesta.push(this.state.respuestaPre03[i]);
      }
      if(this.state.respuestasSeleccionadas.length<3){
        alert("Seleccione TRES preguntas para el entrevistado e intente de nuevo");
        return false;
      }
      for(let i=0;i<this.state.respuesta.length;i++){
        for(let j=0;j<this.state.respuestasSeleccionadas.length;j++){
          // eslint-disable-next-line
          if(i==this.state.respuestasSeleccionadas[j]){
            if(this.state.respuesta[j]===null||this.state.respuesta[j]==="" ||this.state.respuesta[j]===" "){
              alert("Revise que sus respuestas seleccionadas no esten en blanco e intente de nuevo");
              return false;
            }
            respuesta.push(this.state.respuesta[i]);
          }
        }
      }

      for(let i=0;i<this.state.respuesta.length;i++){
        if(i===10||i===8||i===9){
            if(this.state.respuesta[i]===null||this.state.respuesta[i]==="" ||this.state.respuesta[i]===" "){
            alert("Revise que las acciones para el siguiente One on One no esten en blanco e intente de nuevo");
            return false;
          }
          respuesta.push(this.state.respuesta[i]);
        }
      }
  
      let answers=respuesta;
      let personId=this.state.selectedPerson;
     let date=this.state.datosPersonales[2]
      let topicChecklist=this.state.topicos;
      if(topicChecklist<1){
        alert("Seleccione al menos una opcion de entre los topicos de las preguntas Pre One on One");
        return false;

      }
      let questionIndex = this.state.respuestasSeleccionadas;

      url=url+formId+'&answers='+encodeURIComponent(answers)+'&preO3Questions='+encodeURIComponent(preO3Questions)+'&topicChecklist='+topicChecklist+'&questionIndex='+encodeURIComponent(questionIndex)+'&personId='+encodeURIComponent(personId)+'&date='+date;
      fetch(url,{method: 'POST', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
          }})
          .then(res=>window.location="/index",(error)=>{console.log(error)});    
    }
  
    recuperarGrupos(){
      let token= sessionStorage.Token;
      let username= sessionStorage.Nombre;
      let url= 'http://localhost:8090/getAllTeams?username='+username;
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
         .then((gruposRecuperados) => {
            for(let i=0;i<JSON.parse(gruposRecuperados).length; i++){
              let id =this.state.id.slice();
              id.push(JSON.parse(gruposRecuperados)[i].id);
              let nombreDelEquipo =this.state.nombreDelEquipo.slice();
              nombreDelEquipo.push(JSON.parse(gruposRecuperados)[i].name);
              this.setState({ 
                id: id ,
                nombreDelEquipo: nombreDelEquipo
              });
            }
        },
          (error) => {
            console.log(error);
          });
    }
   handleChange(i,e){ 
      let topicos=this.state.topicos.slice();
      let tempo2=Array(0);
     
      if(e.target.checked){
        // eslint-disable-next-line
            if(e.target.value==1){
              
              topicos.push("Metas");
            }// eslint-disable-next-line
            if(e.target.value==2){
              topicos.push("Desarrollo");
            }// eslint-disable-next-line
            if(e.target.value==3){
              topicos.push("Preocupaciónes");
            }// eslint-disable-next-line
            if(e.target.value==4){
              topicos.push("Necesidad de ayuda");
            }// eslint-disable-next-line
            if(e.target.value==5){
              topicos.push("Intereses futuros");
            }// eslint-disable-next-line
            if(e.target.value==6){
              topicos.push("Item de accion");
            }
            this.setState({
              temporal:topicos,
              topicos:topicos
            });

          }else {
            // eslint-disable-next-line
            if(1==e.target.value){
              
              for(let i=0;i<topicos.length;i++){
                if(topicos[i]!=="Metas")
                {
                  tempo2.push(topicos[i]);
                  
                } 
              }
              this.setState({topicos:tempo2});
            }
             // eslint-disable-next-line
            if(2==e.target.value){
            
              for(let i=0;i<topicos.length;i++){
                if(topicos[i]!=="Desarrollo")
                {
                  tempo2.push(topicos[i]);
                } 
                
              }
              this.setState({topicos:tempo2});
            }// eslint-disable-next-line
            if(3==e.target.value){
              
              for(let i=0;i<topicos.length;i++){
                if(topicos[i]!=="Preocupaciónes")
                {
                  tempo2.push(topicos[i]);
                }  
                
              }
              this.setState({topicos:tempo2});
            } // eslint-disable-next-line
            if(4==e.target.value){
              
              for(let i=0;i<topicos.length;i++){
                if(topicos[i]!=="Necesidad de ayuda")
                {
                  tempo2.push(topicos[i]);
                } 
              }
              this.setState({topicos:tempo2});
            }
            // eslint-disable-next-line
            if(5==e.target.value){
              
              for(let i=0;i<topicos.length;i++){
                if(topicos[i]!=="Intereses futuros")
                {
                  tempo2.push(topicos[i]);
                } 
              }
              this.setState({topicos:tempo2});
            }
            // eslint-disable-next-line
            if(6==e.target.value){
              for(let i=0;i<topicos.length;i++){
                if(topicos[i]!=="Item de accion")
                {
                  tempo2.push(topicos[i]);
                } 
              }
              this.setState({topicos:tempo2});
            } 

              this.setState({
                temporal:topicos,
              });
            }
          }

  render() {
    return(
    <div>
        <NavBar></NavBar>
          <BackButton volver="/index"></BackButton>
          <TipsForaAnEffectiveOneOnOne/>
         <form>
        <Paper className='paper'>
            <div className='BasicQuestions'>
            <Typography variant="h5" component="h3">
            Datos del entrevistado
              </Typography>  
                <div> 
                  <Paper className="date">
                   <TeamSelect ids={this.state.id} nombresDeEquipos={this.state.nombreDelEquipo} onChange={this.onChangeData.bind(this,1)} ></TeamSelect>  
                   <PersonSelect ids={this.state.idPersonas} personas={this.state.personas} onChange={this.onChangeData.bind(this,0)} ></PersonSelect> 
                  </Paper>
                  <br></br>
                  <Paper className="dateBox">
                  <TextField

                    label="Fecha de la entrevista"
                    type="date"
                    defaultValue={this.date}
                    id="date"
                    onChange={this.onChangeData.bind(this,2)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                 </Paper>
                 </div>        
               </div> 
               <br></br>
               <div>
               <Paper className={"paper1"} >
                 
                  <Typography variant="h5" component="h3">
                  Notas Pre One-on-One:
                  </Typography>
                  <Typography  component="p">
                  Escriba 3 preguntas para asegurar una sesión efectiva con el empleado:
                  </Typography>
                  <br></br>
                  <TextField
                    id="0"
                    label="Ingrese la primera pregunta"
                    fullWidth
                    onChange={this.anadirPreguntaO3.bind(this,0)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                   <TextField
                    id="1"
                    label="Ingrese la repuesta a la primera pregunta"
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    onChange={this.anadirRespuestaPreO3.bind(this,0)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="2"
                    label="Ingrese la segunda pregunta"
                    fullWidth
                    margin="normal"
                    onChange={this.anadirPreguntaO3.bind(this,1)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="3"
                    label="Ingrese la respuesta a la segunda pregunta"
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    onChange={this.anadirRespuestaPreO3.bind(this,1)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="4"
                    label="Ingrese la tercera pregunta"
                    fullWidth
                    onChange={this.anadirPreguntaO3.bind(this,2)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="5"
                    label="Ingrese la respuesta a la tercera pregunta"
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    onChange={this.anadirRespuestaPreO3.bind(this,2)}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                   <br></br>
                  <Typography variant="h5" component="h3">
                  Verificación de tópicos
                  </Typography>
                
                  <FormControl component="fieldset" className='formControl'>
                    <FormLabel component="legend">  Seleccione los tópicos que cubren las preguntas anteriores: </FormLabel>
                   <FormGroup onChange={this.handleChange.bind(this,1)}>
               <FormControlLabel
                control={<Checkbox   value="1" name="Metas" />}
                label="Metas"/>
              <FormControlLabel
                control={<Checkbox   value="2"   name="Desarrollo" />}
                label="Desarrollo"/>
              <FormControlLabel
                control={<Checkbox   value="3"  name="Preocupaciones" />}
                label="Preocupaciones"/>
              <FormControlLabel
                control={<Checkbox   value="4"  name="Se necesita ayuda" />}
                label="Se necesita ayuda"/>
              <FormControlLabel
                control={<Checkbox   value="5"  name="Intereses futuros" />}
                label="Intereses futuros"/>
              <FormControlLabel
                control={<Checkbox   value="6"  name="Items de accion"  />}
                label="Items de accion" />
            </FormGroup>
          </FormControl>

                </Paper>
                 </div>  
                 <br></br>
  
              <Typography variant="h5" component="h3">
              Preguntas para el entrevistado
              </Typography> 
              <Typography  component="p">
                  Seleccione las 3 preguntas que le realizo al entrevistado:
              </Typography>
              <br></br>
                {this.generarLista()}                
               <br></br>
                <Paper className={"paper1"}>
              <Typography variant="h5" component="h3">
                 Acciones para el siguiente One on One
              </Typography>  
              <br></br>
               <strong>
               {this.generarLista2()}
                 </strong>  
                  </Paper> 
                <Button variant="contained" onClick={this.handleSave}>Guardar</Button>      
            </Paper>
          </form>
    </div>
    )
  }
}