import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import NavBar from '../NavBar';
import BackButton from '../backButton/backButton';

import { CardContent, Typography } from '@material-ui/core';

export default class ManagePerson extends Component {
  
  constructor(props){
      super(props);
      this.state={
        form:''
      };
    }
    componentDidMount(){
     this.loged();
    }
    
    loged() {
      let token= sessionStorage.Token;
      let url= 'http://localhost:8090/loged?';
      let name= sessionStorage.Nombre;
      if(name===undefined){
        url+="username=null";  
      }
      else{
      url+="username="+name; 
      } 
      fetch(url,{method: 'GET', credentials: 'same-origin' ,
        headers: {
           "Authorization": token,
           "Access-Control-Allow-Origin": '*', 
         }}).then(res => res.text())
      .then((data) => {
        if(data==="true"){
            return true;
        }else{  
          window.location="/";  
        }
      },
      (error) => {
        
      });
    }
   
    render() {
      return (
        <div className="App_Format">
        <NavBar></NavBar>
        <BackButton  volver={"/administrar"}></BackButton>

        <div className='cardContainer'>     
       <Card >
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant="h6" component="h2">
            Nueva Persona
            </Typography>
           
            </CardContent>
          <Button  className="button1" href='/nuevaPersona' size="small" color="primary">
          <CardMedia
            component="img"
            image='https://image.flaticon.com/icons/svg/554/554871.svg'
            title="Nueva Persona" 
          />
          </Button>
  
        </CardActionArea>
      </Card>
      <Card >
         <CardActionArea>
            <CardContent>
            <Typography gutterBottom variant="h6" component="h2">
            Ver Persona
            </Typography>
            
          </CardContent>
          <Button  className="button1" href='/verPersona' size="small" color="primary">
          <CardMedia
          component="img"
          image='https://image.flaticon.com/icons/svg/554/554835.svg'
          title="Ver Persona" 
          />
          </Button>
          </CardActionArea>
      </Card>
    
      </div>
        </div>
        
      )
    }
  }
  
  
  