import React, { Component } from 'react'
import './checkboxInput.css'
import '../CheckBox/CheckBox.css'
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import AddCheck from '../AddCheck/AddCheck';
import {Paper} from '@material-ui/core';
import Textofinput from '../textofinput/textofinput';


export default class CheckboxInput extends Component {
  constructor(props){
    super(props);
    this.state = {
      data : [
        {
          
        }
      ],
      respuesta:Array(1).fill(null),
      opcion:Array(1).fill(null),
      input: 'check',
      index: this.props.index,
      indexOption:1
    };
    //props=this.state.respuesta;
    }
  
  _remove(position){
    let { data } = this.state;
  
    let newData = [
      ...data.slice(0, position),
      ...data.slice(position + 1),
    ]
     this.setState({ data : newData ,
    });
  
  }
  _add(){
    let { data } = this.state;
    let newData = [
      ...data,
      {
        respuesta  : '',
        input : "",
      }
    ];
    let index=this.generarLista().length;
    this.setState({indexOption:index ,data : newData });
  }
  onChange(i,e){
    let respuesta=this.state.respuesta.slice() ;
    respuesta[i]=e.target.value;
    this.setState({respuesta: respuesta,});
    this.props.respuesta[i]=respuesta[i];
    console.log("hola"+this.props.indexOpcion);
  }
  generarLista(){
    let i=-1;
    let listaDePreguntas=  this.state.data.map(index =>
            <li key={i++}>
     
              <FormControlLabel
            control={<Textofinput onRemove={ () => this._remove(i)} onChange={this.onChange.bind(this,i)} input={this.props.input} respuesta={this.state.respuesta}/>}
            />   
            </li>
      )
   
        return listaDePreguntas
  }
          
        
  render() {
    return(
      <div>
        <h3>Ingrese posibles respuestas: </h3>
        <Paper>
      
        <ul >
      <FormGroup row>
        {this.generarLista()}
      </FormGroup >
        </ul>  

      <AddCheck  onClick={this._add.bind(this)} >Añadir Respuesta</AddCheck>
        </Paper>

      </div>
    )
  }
}