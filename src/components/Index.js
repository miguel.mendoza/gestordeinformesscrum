import React, {Component} from 'react';
import './Index.css';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import NavBar from './NavBar';
import { CardContent, Typography } from '@material-ui/core';

export default class Index extends Component {
  constructor(props){
    super(props);
    this.state={
      form:''
    };
  }
  componentDidMount(){
    this.loged();
  }
  
  loged() {
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/loged?';
    let name= sessionStorage.Nombre;
    if(name===undefined){
      url+="username=null";  
    }
    else{
    url+="username="+name; 
    } 
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
    .then((data) => {
      if(data==="true"){
          return true;
      }else{  
        window.location="/";  
      }
    },
    (error) => {
      
    });
  }
 
  render() {
    return (
      <div className="App_Format">
      <NavBar></NavBar>
     
      <div className='cardContainer'>     
     <Card >
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
            Llenar Fromulario
          </Typography>
         
          </CardContent>
        <Button  className="button1" href='/llenarFormulario' size="small" color="primary">
        <CardMedia
          component="img"
          image='https://image.flaticon.com/icons/svg/220/220922.svg'
          title="Llenar Formulario" 
        />
        </Button>

      </CardActionArea>
    </Card>
    <Card >
       <CardActionArea>
          <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
            Crear Fromulario
          </Typography>
          
        </CardContent>
        <Button  className="button1" href='/nuevo' size="small" color="primary">
        <CardMedia
        component="img"
        image='https://image.flaticon.com/icons/svg/220/220941.svg'
        title="Crear Formulario" 
        />
        </Button>
        </CardActionArea>
    </Card>
    <Card >
          <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
            Ver Respuestas
          </Typography>
          
        </CardContent>
        <Button  className="button1" href='/verRespuestas' size="small" color="primary">
        <center>
        <CardMedia
        component="img"
        image='https://image.flaticon.com/icons/svg/220/220930.svg'
        title="Ver Respuestas de mis formularios" 
        />
        </center>
        </Button>
        </CardActionArea>
    </Card>
    <Card >
          <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
            Gestionar
          </Typography>
          
        </CardContent>
        <Button  className="button1" href='/administrar' size="small" color="primary">
        <center>
        <CardMedia
        component="img"
        image='https://image.flaticon.com/icons/svg/1006/1006536.svg'
        title="Gestionar" 
        />
        </center>
        </Button>
        </CardActionArea>
    </Card>
    
    </div>
      </div>
      
    )
  }
}


