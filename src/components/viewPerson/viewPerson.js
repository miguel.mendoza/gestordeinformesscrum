import React, { Component } from 'react'
import './viewPerson.css'
import BackButton from '../backButton/backButton';
import NavBar from '../NavBar';
import { Paper, Typography } from '@material-ui/core';

export default class ViewPerson extends Component {
  constructor(props){
    super(props);
    this.state={
      idDeTdasLasPersonas:Array(0),
      todasLasPersonas:Array(0)
    }
  }
  loged() {
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/loged?';
    let name= sessionStorage.Nombre;
    if(name===undefined){
      url+="username=null";  
    }
    else{
    url+="username="+name; 
    } 
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
    .then((data) => {
      if(data==="true"){
          return true;
      }else{  
        window.location="/";  
      }
    },
    (error) => {
      console.log(error);
    });
  }
  componentDidMount(){
    this.loged();
    this.recuperarTodasLasPersonas();
  }
  recuperarTodasLasPersonas(){
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/getAllPerson';
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
       .then((personasRecuperadas) => {
         let personas= Array(0);
         let idPersonas= Array(0);
        for(let i=0;i<JSON.parse(personasRecuperadas).length; i++){
          idPersonas.push(JSON.parse(personasRecuperadas)[i].id);
          personas.push(JSON.parse(personasRecuperadas)[i].name+" "+JSON.parse(personasRecuperadas)[i].lastName);
          this.setState({ 
            idDeTdasLasPersonas: idPersonas ,
            todasLasPersonas: personas
          });
        }
      },
        (error) => {
          console.log(error);
        });
  }
  generarLista(){
      let i=-1;
      let listaPersona= this.state.todasLasPersonas.map((persona) =>
         <Paper>  
          <li key={"g"+i++}>
           <div>
        <strong>
        {persona}
        </strong>
           </div>
           </li>      
        </Paper>   
      )
      
    return listaPersona;
    
  }
  render() {
    return(
      <div>
        <NavBar></NavBar>
          <BackButton volver={"/personas"}></BackButton>
          <div >
            <Paper className="paper">
            <Typography variant="h5" component="h3">
            Personas registradas
            </Typography> 
            <Paper >
            {this.generarLista()}
              </Paper>  
            </Paper>
          </div>
      </div>
    )
  }
}