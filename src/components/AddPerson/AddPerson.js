import React, { Component } from 'react';
import './AddPerson.css';
import BackButton from '../backButton/backButton';
import NavBar from '../NavBar';
import { Paper, TextField, Button } from '@material-ui/core';


export default class AddPerson extends Component {
  constructor(props){
    super(props);
    this.state = {
      nombre: '',
      apellido: ''
    };
    this.flagApellido=this.flagApellido;
    this.flagApellido=true;
    this.flagNombre=this.flagNombre;
    this.flagNombre=true;
  this.onChange=this.onChange.bind(this);
  }
  loged() {
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/loged?';
    let name= sessionStorage.Nombre;
    if(name===undefined){
      url+="username=null";  
    }
    else{
    url+="username="+name; 
    } 
    fetch(url,{method: 'GET', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
       }}).then(res => res.text())
    .then((data) => {
      if(data==="true"){
          return true;
      }else{  
        window.location="/";  
      }
    },
    (error) => {
      console.log(error);
    });
  }
  componentDidMount(){
    this.loged();

  }

  
  onChange(i,e) { 
    if(i===1){
      let nombre=this.state.nombre;
      nombre = e.target.value; 
      if(nombre.length<30){
        this.flagNombre=true;
        this.setState({nombre:nombre});
      }else{
        this.flagNombre=false;
        alert("El nombre es demasiado largo, porfavor ingrese un nombre mas corto");
      }
    }
    if(i===2){
      let apellido=this.state.apellido;
      apellido=e.target.value;
      if(apellido.length<30){
        this.setState({apellido:apellido});
        this.flagApellido=true;
      }else{
        this.flagApellido=false;
        alert("El apellido es demasiado largo, porfavor ingrese un nombre mas corto");
      }
      this.setState({apellido:apellido})
    }
  } 

  handleSave(event) {
    if(this.state.nombre===""){
      alert("Por favor ingrese el nombre"); 
      return false;
    }
    if(this.state.apellido===""){
      alert("Por favor ingrese el apellido"); 
      return false;
    }
    if(this.flagNombre===false){
      alert("No se puede almacenar un nombre tan largo");
      return false;
    }
    if(this.flagApellido===false){
      alert("No se puede almacenar un apellido tan largo")
      return false;
    }
    let token= sessionStorage.Token;
    let url= 'http://localhost:8090/savePerson?name=';
    let name=encodeURIComponent(this.state.nombre);
    let lastName=encodeURIComponent(this.state.apellido);
    url+=name;
    url+="&lastName="+lastName;  
    fetch(url,{method: 'POST', credentials: 'same-origin' ,
      headers: {
         "Authorization": token,
         "Access-Control-Allow-Origin": '*', 
        }})
        .then((error)=>{console.log(error)});
        window.location="/personas";
  }
  render() {
    return(
      <div>
      <NavBar></NavBar>
          <BackButton volver="/personas"></BackButton>
            
          <div className="paper">
              <Paper>
           <h1 styles="margin-right:3em;">Datos de la Persona</h1>
              <TextField
              id="outlined-name"
              label="Nombre"
              onChange={this.onChange.bind(this,1)}
              variant="outlined"
            />
             <TextField
              id="outlined-name"
              label="Apellido"
              onChange={this.onChange.bind(this,2)}
              variant="outlined"
            />
            <div className="buttonBox">
                <Button className="save" onClick={this.handleSave.bind(this)}>Guardar Persona</Button>
            </div>
                  </Paper>
          </div>
  </div>
    )
  }
}