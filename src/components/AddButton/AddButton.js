import React, { Component } from 'react'
import './AddButton.css'
import Button from '@material-ui/core/Button';


export default class AddButton extends Component {
    _add(){
        if(this.props.onClick)
            this.props.onClick();
    }
 
    render(){
        return (
          <Button className="AddButton" onClick={this._add.bind(this)}>Añadir Pregunta</Button>
          
        )
    }
}
 
